import ApplicationAdapter from './application';
import { getOwner } from '@ember/application';


export default class JobAdapter extends ApplicationAdapter {
    get host() {
        return this._apiHost;
    }

    get _apiHost() {
        return getOwner(this).application.scheduleApiHost;
    }

    get namespace() {
        return '';
    }
}
