import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';


export default class IndexIndexRoute extends Route {
    @service session;

    beforeModel(transition) {
        this.session.requireAuthentication(transition, 'login');
    }

    model() {
        return hash({
            jobs: this.store.findAll('job'),
        });
    }
}
