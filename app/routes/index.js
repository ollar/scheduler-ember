import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { action } from "@ember/object";
import { later } from '@ember/runloop';

export default class IndexRoute extends Route {
    @service me;
    @service radio;
    @service session;

    model() {
        return this.me.fetch();
    }

    @action
    error(error, transition) {
        transition.abort();
        this.radio.send({
            type: 'error',
            text: error.message,
        });

        later(() => this.transitionTo('logout'), 1000);
    }
}
