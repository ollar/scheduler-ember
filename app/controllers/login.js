import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';


export default class LoginController extends Controller {
    @service session;
    @service radio;

    @action
    updateModelValue(e) {
        if (e && e.preventDefault) e.preventDefault();
        const { value, name } = e.target;

        if (name) this.model.set(name, value);
    }

    @action
    submit(e) {
        if (e && e.preventDefault) e.preventDefault();

        if (this.model.validate()) {
            const data = this.model.serialize();
            this.session.authenticate('authenticator:application', data)
                .then(() => this.transitionToRoute('index'))
                .catch(e => {
                    this.radio.send({
                        type: 'error',
                        text: e.detail
                    });
                })
        }
    }
}
