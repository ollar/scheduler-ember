import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';


export default class RegisterController extends Controller {
    @service session;

    @action
    updateModelValue(e) {
        if (e && e.preventDefault) e.preventDefault();
        const { value, name } = e.target;

        if (name) this.model.set(name, value);
    }

    @action
    submit(e) {
        if (e && e.preventDefault) e.preventDefault();

        const data = this.model.serialize();

        if (this.model.validate()) {
            return this.session.register(data)
                .then(() => this.session.authenticate('authenticator:application', data))
                .then(() => {
                    const user = this.store.createRecord('user', {
                        username: data.username,
                        email: data.email,
                    });

                    return user.save();
                })
                .then(() => this.transitionToRoute('index'))
                .catch(error => {
                    Object.keys(error).forEach(key =>
                        this.radio.send({
                            type: 'error',
                            text: error[key]
                        })
                    );
                });
        }
    }
}
