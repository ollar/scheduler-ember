import Model, { attr, hasMany} from '@ember-data/model';

export default class UserModel extends Model {
    @attr username;
    @attr email;

    @attr _uid;

    @hasMany('jobs') jobs;
}
