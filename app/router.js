import EmberRouter from '@ember/routing/router';
import config from 'scheduler/config/environment';

export default class Router extends EmberRouter {
    location = config.locationType;
    rootURL = config.rootURL;
}

Router.map(function () {
    this.route('index', { path: '/' }, function () {
        this.route('index', { path: '/' });
    });
    this.route('login');
    this.route('logout');
    this.route('register');
});
